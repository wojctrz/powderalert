/**
 * 2021 WOWO No rights reserved
*/

#include <iostream>
#include <memory>

#include "OwmWeatherApiConnector.hpp"
#include "JsonConfigReader.hpp"
#include "PowderChecker.hpp"
#include "ConsolePrintObserver.hpp"
#include "LocationData.hpp"

#include "nlohmann/json.hpp"


int main(int argc, char** argv)
{
    std::string configFilePath;
    if(argc < 2)
    {
        std::cout << "No config file provided. Assuming \"config.json\" in the working directory\n";
        configFilePath = "config.json";
    }
    else
    {
        configFilePath = argv[1];
    }
    JsonConfigReader config(configFilePath);
    std::string apiKey = config.getApiKey();
    std::vector<LocationData> locations = config.getAllCoordinates();
    std::unique_ptr<OwmWeatherApiConnector> owapc = std::make_unique<OwmWeatherApiConnector>(apiKey);

    PowderChecker pcheck(std::move(owapc), locations);

    std::unique_ptr<ConsolePrintObserver> firstObserver = std::make_unique<ConsolePrintObserver>();

    pcheck.attachObserver(std::move(firstObserver));

    pcheck.checkForPowder();


    // OwmWeatherApiConnector owapc(apiKey);
    // try
    // {
    //     auto doopa = owapc.getHourlyForecastForLocation(1, 1);

    //     for(auto& xd : doopa)
    //     {
    //         std::cout << xd.temperature << "  ";
    //     }

    // }
    // catch(const std::exception& e)
    // {
    //     std::cerr << "Lol " << e.what() << '\n';
    // }
    

}
