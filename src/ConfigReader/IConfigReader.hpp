#ifndef ICONFIGREADER_HPP
#define ICONFIGREADER_HPP

#include <string>
#include <vector>
#include <utility>

#include "LocationData.hpp"

class IConfigReader
{
public:
    virtual std::string getApiKey() = 0;
    virtual std::vector<LocationData> getAllCoordinates() = 0;
};

#endif // ICONFIGREADER_HPP
