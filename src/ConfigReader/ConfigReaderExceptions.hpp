#ifndef CONFIGREADEREXCEPTIONS_HPP
#define CONFIGREADEREXCEPTIONS_HPP

#include <string>
#include <exception> // exception


class ConfigReaderException : public std::exception
{
public:
    ConfigReaderException(std::string description) : desc(description) {}

    const char* what() const noexcept override
    {
        return desc.c_str();
    }

private:
    std::string desc;
};

class ConfigFileOpenException : public std::exception
{
public:
    ConfigFileOpenException(std::string fileName)
    {
        desc = "Unable to open config file " + fileName;
    }

    const char* what() const noexcept override
    {
        return desc.c_str();
    }

private:
    std::string desc;
};

#endif // CONFIGREADEREXCEPTIONS_HPP
