#include <iostream>
#include <fstream>
#include <sstream>

#include "JsonConfigReader.hpp"
#include "ConfigReaderExceptions.hpp"

using json = nlohmann::json;
JsonConfigReader::JsonConfigReader(std::string configFileName) : configFileName(configFileName)
{
    std::string configString = getConfigFromFile();
    parsedConfig = json::parse(configString);
}

std::string JsonConfigReader::getApiKey()
{
    std::string apiKey;
    try
    {
        apiKey = parsedConfig["apiKey"];
    }
    catch (json::type_error& e)
    {
        std::string errorMsg = std::string("Error while getting API key from config file: ") + std::string(e.what());
        throw ConfigReaderException(errorMsg);
    }
    return apiKey;
}

std::vector<LocationData> JsonConfigReader::getAllCoordinates()
{
    std::vector<LocationData> locations;
    json locationsFromConfig(json::value_t::array);
    try
    {
        locationsFromConfig = parsedConfig["locations"];
    }
    catch (json::type_error& e)
    {
        std::string errorMsg = std::string("Error while getting locations from config file: ") + std::string(e.what());
        throw ConfigReaderException(errorMsg);
    }

    if(locationsFromConfig == nullptr)
    {
        throw ConfigReaderException("Error: there is no \"locations\" entry in the config file");
    }

    for (auto& elem : locationsFromConfig)
    {
        LocationData location(elem[0], elem[1], elem[2]);
        locations.push_back(location);
    }

    return locations;
}

std::string JsonConfigReader::getConfigFromFile()
{
    std::ifstream file(configFileName);
    std::string config;
    if(file)
    {
        std::ostringstream ss;
        ss << file.rdbuf();
        config = ss.str();
        file.close();
    }
    else
    {
        throw ConfigFileOpenException(configFileName);
    }
    return config;
}
