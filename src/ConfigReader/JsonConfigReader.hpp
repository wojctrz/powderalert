#ifndef JSONCONFIGREADER_HPP
#define JSONCONFIGREADER_HPP

#include <string>
#include <utility>

#include "nlohmann/json.hpp"

#include "IConfigReader.hpp"

class JsonConfigReader : public IConfigReader
{
public:
    JsonConfigReader() = default;
    JsonConfigReader(std::string configFileName);
    std::string getApiKey();
    std::vector<LocationData> getAllCoordinates();
private:
    std::string configFileName;
    nlohmann::json parsedConfig;

    std::string getConfigFromFile();
};

#endif // JSONCONFIGREADER_HPP
