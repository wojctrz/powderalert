#ifndef LOCATIONDATA_HPP
#define LOCATIONDATA_HPP

#include <string>

// Data type for location data

struct LocationData
{
    LocationData(std::string name, double lon, double lat) : name(name), lon(lon), lat(lat) {}
    LocationData() : name(""), lon(0), lat(0) {}
    LocationData(const LocationData&) = default;
    LocationData(LocationData&&) = default;
    LocationData& operator=(LocationData&&) = default;
    LocationData& operator=(const LocationData&) = default;


    std::string name;
    double lon;
    double lat;
};

inline bool operator==(const LocationData& left, const LocationData& right)
{
    return ((left.name == right.name) && (left.lon == right.lon) && (left.lat == right.lat));
}


#endif // LOCATIONDATA_HPP