#ifndef IWEATHEREVENTOBSERVER_HPP
#define IWEATHEREVENTOBSERVER_HPP

#include <string>
#include <ctime>

// Observer of weather events (Yes I mean the POW!)

class IWeatherEventObserver
{
public:
    virtual void Update(std::string locationName, time_t date, double snowVolume) = 0;
};

#endif // IWEATHEREVENTOBSERVER_HPP