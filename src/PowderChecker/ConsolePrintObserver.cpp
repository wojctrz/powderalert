#include "ConsolePrintObserver.hpp"

#include <iostream>
#include <ctime>

void ConsolePrintObserver::Update(std::string locationName, time_t date, double snowVolume)
{
    std::cout << "Warning!!! We have snow!!!\n";
    std::cout << locationName << " In " << convertTimestampToString(date) << " " << snowVolume << " mm of POW\n";
}

std::string ConsolePrintObserver::convertTimestampToString(time_t& timestamp)
{
    std::tm *timeStruct = localtime(&timestamp);
    char buffer[25];
    std::strftime(buffer, sizeof(buffer), "%d.%b.%Y, %H:%M", timeStruct);
    return std::string(buffer);
}