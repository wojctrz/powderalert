#ifndef CONSOLEPRINTOBSERVER_HPP
#define CONSOLEPRINTOBSERVER_HPP

#include <string>

#include "IWeatherEventObserver.hpp"

class ConsolePrintObserver : public IWeatherEventObserver
{
public:
    void Update(std::string locationName, time_t date, double snowVolume);
private:
    std::string convertTimestampToString(time_t& timestamp);
};

#endif // CONSOLEPRINTOBSERVER_HPP