#include "PowderChecker.hpp"

#include <iostream>

#include "WeatherApiConnector/WeatherData.hpp"

PowderChecker::PowderChecker(std::unique_ptr<IWeatherApiConnector> weatherApiIface, std::vector<LocationData> locations)
{
    this->locations = locations;

    weatherApiInterface = std::move(weatherApiIface);
}

void PowderChecker::attachObserver(std::unique_ptr<IWeatherEventObserver> observer)
{
    observers.push_back(std::move(observer));
}

void PowderChecker::checkForPowder()
{
    for (auto& location : locations)
    {
        std::vector<WeatherData> weatherForecast = weatherApiInterface->
            getHourlyForecastForLocation(location.lon, location.lat);

        for (auto& currentWeather : weatherForecast)
        {
            if (currentWeather.snowVolume > powderDetectionTreshold)
            {
                for (auto& obs : observers)
                {
                    obs->Update(location.name, currentWeather.date, currentWeather.snowVolume);
                }
            }
        }
    }
}