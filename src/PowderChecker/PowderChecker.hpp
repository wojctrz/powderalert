#ifndef POWDERCHECKER_HPP
#define POWDERCHECKER_HPP

#include <vector>
#include <memory>
#include <utility>

#include "WeatherApiConnector/IWeatherApiConnector.hpp"
#include "ConfigReader/LocationData.hpp"
#include "IWeatherEventObserver.hpp"

class PowderChecker
{
public:
    PowderChecker(std::unique_ptr<IWeatherApiConnector> weatherApiIface, std::vector<LocationData> locations);
    void attachObserver(std::unique_ptr<IWeatherEventObserver> observer);
    void checkForPowder();

private:
    std::unique_ptr<IWeatherApiConnector> weatherApiInterface;
    std::vector< std::unique_ptr<IWeatherEventObserver> > observers;
    std::vector<LocationData> locations;
    const double powderDetectionTreshold = 0.2;
};

#endif // POWDERCHECKER_HPP