#include <curlpp/cURLpp.hpp>
#include <curlpp/Easy.hpp>
#include <curlpp/Options.hpp>

#include "HttpGetter.hpp"
#include "WeatherApiConnectorExceptions.hpp"


using namespace curlpp::options;

std::stringstream HttpGetter::getHttp(std::string url)
{
    std::stringstream response;
    try
    {
        // That's all that is needed to do cleanup of used resources (RAII style).
        curlpp::Cleanup myCleanup;

        response << curlpp::options::Url(url);
    }
    catch(std::exception & e)
    {
        std::string errorMsg = std::string("Error while sending GET request: ") + std::string(e.what());
        throw HtmlRequestException(errorMsg);
    }

    return response;
}