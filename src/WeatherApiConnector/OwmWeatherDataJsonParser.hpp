#ifndef OWMWEATHERDATAJSONPARSER_HPP
#define OWMWEATHERDATAJSONPARSER_HPP

#include <vector>
#include <sstream>

#include "WeatherData.hpp"

class OwmWeatherDataJsonParser
{
public:
    std::vector<WeatherData> parseWeatherData(std::stringstream& requestJson);
};

#endif //OWMWEATHERDATAJSONPARSER_HPP
