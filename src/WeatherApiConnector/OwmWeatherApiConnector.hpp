#ifndef OWMWEATHERAPICONNECTOR_HPP
#define OWMWEATHERAPICONNECTOR_HPP
// Weather API connector for OpenWeatherMap service

#include "IWeatherApiConnector.hpp"
#include "HttpGetter.hpp"

class OwmWeatherApiConnector : public IWeatherApiConnector
{
public:
    OwmWeatherApiConnector(std::string apiKey);
    std::vector<WeatherData> getHourlyForecastForLocation(double lon, double lat);

private:
    std::string apiKey;
    HttpGetter httpGetter;
    const std::string forecastBaseUrl = "https://api.openweathermap.org/data/2.5/onecall";
    const std::string hourlyForecastExcludes = "&exclude=current,minutely,daily,alerts";
    const std::string forecastMetricUnits = "&units=metric";

    std::string constructFullRequestUrl(double lon, double lat);

};

#endif //OWMWEATHERAPICONNECTOR_HPP
