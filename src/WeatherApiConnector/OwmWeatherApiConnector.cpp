#include <iostream>
#include <sstream>


#include "OwmWeatherApiConnector.hpp"
#include "OwmWeatherDataJsonParser.hpp"




OwmWeatherApiConnector::OwmWeatherApiConnector(std::string apiKey) : apiKey(apiKey) {}

std::vector<WeatherData> OwmWeatherApiConnector::getHourlyForecastForLocation(double lon, double lat)
{
    std::stringstream responseJson;

    std::string requestUrl = constructFullRequestUrl(lon, lat);

    responseJson = httpGetter.getHttp(requestUrl);

    OwmWeatherDataJsonParser dataParser;

    std::vector<WeatherData> hourlyForecast = dataParser.parseWeatherData(responseJson);

    return hourlyForecast;
}

std::string OwmWeatherApiConnector::constructFullRequestUrl(double lat, double lon)
{
    std::string fullRequestUrl = forecastBaseUrl + "?lat=" + std::to_string(lat) + "&lon=" + std::to_string(lon)
        + hourlyForecastExcludes + "&appid=" + apiKey + forecastMetricUnits;

    return fullRequestUrl;
}
