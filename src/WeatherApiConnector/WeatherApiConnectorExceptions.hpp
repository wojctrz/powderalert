#ifndef WEATHERAPICONNECTOREXCEPTIONS_HPP
#define WEATHERAPICONNECTOREXCEPTIONS_HPP

#include <string>
#include <exception> // exception


class ForecastParseException : public std::exception
{
public:
    ForecastParseException(std::string description) : desc(description) {}

    const char* what() const noexcept override
    {
        return desc.c_str();
    }

private:
    std::string desc;
};

class ForecastRequestException : public std::exception
{
public:
    ForecastRequestException(std::string description) : desc(description) {}

    const char* what() const noexcept override
    {
        return desc.c_str();
    }

private:
    std::string desc;
};

class HtmlRequestException : public std::exception
{
public:
    HtmlRequestException(std::string description) : desc(description) {}

    const char* what() const noexcept override
    {
        return desc.c_str();
    }

private:
    std::string desc;
};

#endif // WEATHERAPICONNECTOREXCEPTIONS_HPP
