#ifndef WEATHERDATA_HPP
#define WEATHERDATA_HPP

#include <ctime>

struct WeatherData
{
    WeatherData(time_t date, double temperature, double rainVolume, double snowVolume) :
        date (date), temperature(temperature), rainVolume(rainVolume), snowVolume(snowVolume)
        {}
    WeatherData() : date (0), temperature(0), rainVolume(0), snowVolume(0) {}
          // TODO fix it somehow in some nice way idk lol
    WeatherData(const WeatherData&) = default;
    WeatherData(WeatherData&&) = default;
    WeatherData& operator=(WeatherData&&) = default;
    WeatherData& operator=(const WeatherData&) = default;

    time_t date;
    double temperature;
    double rainVolume;
    double snowVolume;
};

#endif //WEATHERDATA_HPP
