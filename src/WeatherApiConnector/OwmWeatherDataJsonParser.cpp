#include <iostream>
#include <ctime>

#include "nlohmann/json.hpp"

#include "OwmWeatherDataJsonParser.hpp"
#include "WeatherApiConnectorExceptions.hpp"


using json = nlohmann::json;

std::vector<WeatherData> OwmWeatherDataJsonParser::parseWeatherData(std::stringstream& requestJson)
{
    std::vector<WeatherData> parsedData;
    json parsedConfig = json::parse(requestJson);

    json errorCode(json::value_t::number_integer);
    errorCode = parsedConfig["cod"];
    if(errorCode != nullptr)
    {
        json errorMessage(json::value_t::string);
        errorMessage = parsedConfig["message"];
        std::stringstream errorDescription;
        errorDescription << "Error " << errorCode << ": " << errorMessage;
        throw ForecastRequestException(errorDescription.str());
    }

    json hourlyForecast(json::value_t::array);
    try
    {
        hourlyForecast = parsedConfig["hourly"];
    }
    catch (json::type_error& e)
    {
        std::string errorMsg = std::string("Error while parsing received weather data: ") + std::string(e.what());
        throw ForecastParseException(errorMsg);
    }
    for (auto& elem : hourlyForecast)
    {
        time_t date = elem["dt"];
        double temperature = elem["temp"];
        double rain;
        try
        {
            rain = elem["rain"]["1h"];
        }
        catch (json::type_error& e)
        {
            //that means that there is no rain forecasted
            rain = 0;
        }
        double snow;
        try
        {
            snow = elem["snow"]["1h"];
        }
        catch (json::type_error& e)
        {
            //that means that there is no snow forecasted
            snow = 0;
        }
        WeatherData wd(date, temperature, rain, snow);
        parsedData.push_back(wd);
    }

    return parsedData;
}
