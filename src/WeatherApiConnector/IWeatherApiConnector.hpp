#ifndef IWEATHERAPICONNECTOR_HPP
#define IWEATHERAPICONNECTOR_HPP

#include <string>
#include <vector>

#include "WeatherData.hpp"

class IWeatherApiConnector
{
public:
    virtual std::vector<WeatherData> getHourlyForecastForLocation(double lon, double lat) = 0;

};



#endif //IWEATHERAPICONNECTOR_HPP