#ifndef HTTPGETTER_HPP
#define HTTPGETTER_HPP

#include <string>
#include <sstream>

class HttpGetter
{
public:
    std::stringstream getHttp(std::string url);
};

#endif //HTTPGETTER_HPP