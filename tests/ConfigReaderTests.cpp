#include <string>
#include <fstream>
#include <iostream>

#include <catch/catch.hpp>

#include "ConfigReader/JsonConfigReader.hpp"
#include "ConfigReader/ConfigReaderExceptions.hpp"
#include "ConfigReader/LocationData.hpp"

void prepareConfigJsonFileForTest()
{
    std::string string_json = R"({
    "apiKey": "UlVDSCBSVUNIIEhLUw==",
    "locations": [
      ["Test1", 50.69, 20],
      ["Test2", 60, 30]
    ]
})";
    std::ofstream myfile("example.json");
    myfile << string_json;
    myfile.close();
}

void prepareConfigJsonWithNoApiKey()
{
    std::string string_json = R"({
    "locations": [
      [50.69, 20],
      [60, 30]
    ]
})";
    std::ofstream myfile("examplenoapikey.json");
    myfile << string_json;
    myfile.close();
}

void prepareConfigJsonWithNoLocation()
{
    std::string string_json = R"({
    "apiKey": "UlVDSCBSVUNIIEhLUw=="
})";
    std::ofstream myfile("examplenolocations.json");
    myfile << string_json;
    myfile.close();
}

TEST_CASE( "JSON config reader parsing", "[JsonConfigReader]" )
{
    prepareConfigJsonFileForTest();
    JsonConfigReader jcr("example.json");

    SECTION("Test parsing API key")
    {
        std::string apiKey = jcr.getApiKey();

        REQUIRE(apiKey == "UlVDSCBSVUNIIEhLUw==");
    }
    SECTION( "JSON config reader parse JSON locations array")
    {
        std::vector<LocationData> coors = jcr.getAllCoordinates();

        REQUIRE(coors.size() == 2);

        LocationData first_location = coors[0];
        LocationData expected_first_location("Test1", 50.69, 20);

        LocationData second_location = coors[1];
        LocationData expected_second_location("Test2", 60, 30);

        REQUIRE(first_location == expected_first_location);
        REQUIRE(second_location == expected_second_location);
    }
}

TEST_CASE( "JSON config reader error cases", "[JsonConfigReader]" )
{
    SECTION("Test no API key in config file")
    {
        prepareConfigJsonWithNoApiKey();
        JsonConfigReader jcr("examplenoapikey.json");

        REQUIRE_THROWS_AS(jcr.getApiKey(), ConfigReaderException);
    }
    SECTION("Test no locations in config file")
    {
        prepareConfigJsonWithNoLocation();
        JsonConfigReader jcr("examplenolocations.json");

        REQUIRE_THROWS_AS(jcr.getAllCoordinates(), ConfigReaderException);
    }
    SECTION("Test non existing config file")
    {
        REQUIRE_THROWS_AS(JsonConfigReader("nonexistingconfigfile.json"), ConfigFileOpenException);
    }
}
