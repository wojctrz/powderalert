Catch2 **v2.x** must be installed to run tests.  
See installation instruction [here](https://github.com/catchorg/Catch2/blob/devel/docs/cmake-integration.md#installing-catch2-from-git-repository).  
After cloning Catch2 git repository, you may need to checkout from devel branch to some v2.x tag
```bash
    git tag
        # select some v2 tag from the list 
    git checkout <selected tag>
        # proceed with installation
```
