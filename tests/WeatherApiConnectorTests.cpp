#include <catch/catch.hpp>

#include "WeatherApiConnector/HttpGetter.hpp"
#include "WeatherApiConnector/OwmWeatherApiConnector.hpp"
#include "WeatherApiConnector/OwmWeatherDataJsonParser.hpp"
#include "WeatherApiConnector/WeatherApiConnectorExceptions.hpp"

#include "ConfigReader/JsonConfigReader.hpp"


TEST_CASE( "HTTP Getter - wrong URL", "[HttpGetter]" )
{
    HttpGetter getter;

    std::string wrongUrl = "http://wrongandnonexistingurlfrjsd.com/xdxdxdxd";  //hope this URL does not exist
    REQUIRE_THROWS_AS(getter.getHttp(wrongUrl), HtmlRequestException);
}

TEST_CASE( "OWM weather API connector - happy path", "[OwmApiConnector]" )
{
    std::string apiKey;
    try
    {
        JsonConfigReader config("config.json");
        apiKey = config.getApiKey();
    }
    catch(const std::exception& e)
    {
        FAIL("No config file provided! To run this test, place valid config.json in the executable directory");
    }

    OwmWeatherApiConnector owapc(apiKey);

    std::vector<WeatherData> wd = owapc.getHourlyForecastForLocation(0, 0);

    REQUIRE(wd.size() > 0);

}

TEST_CASE( "OWM weather API connector - wrong coordinates", "[OwmApiConnector]" )
{
    std::string apiKey;
    try
    {
        JsonConfigReader config("config.json");
        apiKey = config.getApiKey();
    }
    catch(const std::exception& e)
    {
        FAIL("No config file provided! To run this test, place valid config.json in the executable directory");
    }

    OwmWeatherApiConnector owapc(apiKey);

    REQUIRE_THROWS_AS(owapc.getHourlyForecastForLocation(91, 0), ForecastRequestException);
    REQUIRE_THROWS_AS(owapc.getHourlyForecastForLocation(0, 181), ForecastRequestException);
    REQUIRE_THROWS_AS(owapc.getHourlyForecastForLocation(-91, 0), ForecastRequestException);
    REQUIRE_THROWS_AS(owapc.getHourlyForecastForLocation(0, -181), ForecastRequestException);

}

